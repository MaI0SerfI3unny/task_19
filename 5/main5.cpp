#include <iostream>
#include <fstream>
using namespace std;

bool startRound(int choose) {
    int col = 0, colSecond = 0;
    string
        line,
        lineAswer,
        answer,
        chooseQuess,
        chooseAnswer;
    ifstream 
        quessContainer,
        answerContainer;
    quessContainer.open("quess.txt", ios::binary);
    answerContainer.open("answer.txt", ios::binary);

    while (getline(answerContainer, lineAswer)) {
        if(choose == col){
            chooseAnswer = lineAswer;
            break;
        }
        col++;
    }

    while (getline(quessContainer, line)) {
        if(choose == colSecond){
            chooseQuess = line;
            break;
        }
        colSecond++;
    }
    cout<<chooseQuess<<" Что это ? "<<endl;
    cin>>answer;
    return answer == chooseAnswer;
}

bool validate(int *arr, int &choose){
    for(int i = 0; i < 13; i++){
        if(choose == arr[i]) return false;
    }
    return true;
}

int roll(int *arr, int &round){ 
    if(arr[0] == 0){
        arr[0] = 1;
        return 0;
    }
    int choose;
    cout<<"Вращайте волчок 1-13"<<endl;
    cin>>choose;
    bool checkValidate = validate(arr,choose);
    if(!checkValidate){
        while(!checkValidate){
            choose == 13 ? choose = 1 : choose += 1;
            checkValidate = validate(arr,choose);
        }
    }
    arr[round-1] = choose;
    cout<<endl;
    return choose-1;
}

int main(){
    int
    round = 1,
    playerScore = 0, 
    publicScore = 0;
    int usedQuess[13]={0,0,0,0,0,0,0,0,0,0,0,0,0};
    while(round != 13){
        if(playerScore == 6 || publicScore == 6) break;

        int choose = roll(usedQuess, round);

        bool result = startRound(choose);
        if(result){
            playerScore++;
            cout<<"Верный ответ\n";
        }else{
            publicScore++;
            cout<<"Неверный ответ\n";
        }
        round++;
    }
    playerScore == 6 ?
        cout<<"Победа игрока"<<endl : cout<<"Победа публики"<<endl;
    return 0;
}