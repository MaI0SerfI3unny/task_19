#include <iostream>
#include <fstream>
using namespace std;

int main(){
    ifstream myfile;
    myfile.open ("list.txt", ios::binary);
    string line;
    int sum = 0, maxPrice = 0;
    string nameMax, surnameMax, dateMax;
    while (getline(myfile, line)) {
        string name,surname,price,date;
        myfile>>name>>surname>>price>>date;
        sum += stoi(price);
        if(stoi(price) > maxPrice) {
            maxPrice = stoi(price);
            nameMax = name;
            surnameMax = surname;
            date = dateMax;
        }
    }
    cout<<"Общая сумма : " << sum <<endl;
    cout<<"Максимальная сумма выплат : " << maxPrice << " - " << nameMax << " " << surnameMax << " " << dateMax;
    myfile.close();
    return 0;
}