#include <iostream>
#include <fstream>
using namespace std;

int main(){
    ifstream myfile;
    string url;
    cout<<"Введите путь к файлу"<<endl;
    cin>>url;
    myfile.open (url, ios::binary);
    if(myfile.is_open()){
        myfile.seekg(0, std::ios::end);
        size_t size = myfile.tellg();
        string buffer(size, ' ');
        myfile.seekg(0);
        myfile.read(&buffer[0], size);
        for(int i = 0; i < size; i++){
            cout<<buffer[i];
        }
        myfile.close();
    }else{
        cerr<<"Файл не был найден"<<endl;
    }
    return 0;
}