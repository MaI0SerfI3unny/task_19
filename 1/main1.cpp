#include <iostream>
#include <fstream>
using namespace std;

int main(){
    ifstream myfile;

    int resultFounded = 0;
    string line,findStr;
    cout<<"Введите строку,которую нужно найти"<<endl;
    cin>>findStr;
    for(auto& c : findStr)
        c = tolower(c);
    myfile.open ("words.txt", ios::binary);
    while (!myfile.eof()) {
        string substring;
        myfile >> substring;
        for(auto& c : substring)
            c = tolower(c);
        if(substring == findStr) resultFounded++;
    }
    cout<<"Найдено совпадений: "<<resultFounded<<endl;
    myfile.close();
    return 0;
}
